; BSD 3-Clause License
; Copyright (c) 2022-2023, Jerome Shidel

; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:

; 1. Redistributions of source code must retain the above copyright notice, this
;    list of conditions and the following disclaimer.

; 2. Redistributions in binary form must reproduce the above copyright notice,
;    this list of conditions and the following disclaimer in the documentation
;    and/or other materials provided with the distribution.

; 3. Neither the name of the copyright holder nor the names of its
;    contributors may be used to endorse or promote products derived from
;    this software without specific prior written permission.

; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
; DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
; CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

; NASM 2.15.05, or later

; *****************************************************************************
%ifndef CORECRT_CORE_INC_INCLUDED ; check that this file has not been included
%define CORECRT_CORE_INC_INCLUDED
; *****************************************************************************
; Primary runtime CRT options
%idefine vcbDirect	00000001b	; Use Direct Video if possible
%idefine vcbScroll	00000010b	; Enable last line auto-scroll (normal)
%idefine vcbCtrlChar 	00000100b	; Print, don't process control chars
%idefine vcbAutoMouse	00001000b	; Automatic mouse pointer visibility
%idefine vcbExitCursor	00010000b	; Automatic restore cursor at Exit
%idefine vcbExitMode	00100000b	; Automatic restore mode & cursor at Exit

%ifdef CRT_MOUSE
	%idefine vcbDefaults	00111011b ; No vcbCtrlChar
%else
	%idefine vcbDefaults	00110011b ; No vcbCtrlChar or vcbAutoMouse
%endif

; Standard text video display mode constants
%idefine tmBW40		0	; 40x25 B/W on Color adapter
%idefine tmCO40		1	; 40x25 Color on Color adapter
%idefine tmBW80		2	; 80x25 B/W on Color adapter
%idefine tmCO80		3	; 80x25 Color on Color adapter
%idefine tmMono		7	; 80x25 on Monochrome adapter
%idefine tmCO132x25	0x55	; 132x25 Color in DOSBox, maybe others
%idefine tmCO132x50	0x54	; 132x50 Color in DOSBox, maybe others

; Foreground and background color constants
%idefine clBlack	0
%idefine clBlue		1
%idefine clGreen	2
%idefine clCyan		3
%idefine clRed		4
%idefine clMagenta	5
%idefine clBrown	6
%idefine clGray		7	; clGray is short for clLightGray
%idefine clLightGray	7

; Foreground color constants
%idefine clDarkGray	8
%idefine clLightBlue	9
%idefine clLightGreen	10
%idefine clLightCyan	11
%idefine clLightRed	12
%idefine clLightMagenta	13
%idefine clYellow	14
%idefine clWhite	15

; Box and Frame line constants
%idefine lnConnect	0	; If intersects another box/line connect them.
%idefine lnNoConnect	00001b	; Do not connect to other lines/boxes
%idefine lnHorizontal   0	; draw a horizontal line
%idefine lnVertical     00010b	; draw a vertical line
%idefine lnSingle	0	; single line or box
%idefine lnSingleHorz	0
%idefine lnSingleVert	0
%idefine lnDoubleHorz	0100b	; double line or box with double top/bottom lines
%idefine lnDoubleVert	1000b   ; double line or box with double side lines
%idefine lnDouble	1100b   ; double line or box with all double lines
; -----------------------------------------------------------------------------
%macro CRTDataBSS_INTERNAL 0
%ifndef CRTDataBSS_DEFINED
%define CRTDataBSS_DEFINED

	CRT:

	CRT.Card:		resb	1
	CRT.Options:		resb    1

	CRT.Tab:
	CRT.HTab:		resb 	1
	CRT.VTab:		resb 	1

	CRT.Max:
	CRT.Max.X:		resb	1
	CRT.Max.Y:		resb 	1

	CRT.WindMin:
	CRT.WindMin.X:		resb	1
	CRT.WindMin.Y:		resb 	1

	CRT.WindMax:
	CRT.WindMax.X:		resb	1
	CRT.WindMax.Y:		resb 	1

	CRT.PageAttr:
	CRT.Attr:		resb	1
	CRT.Page:		resb	1
	CRT.Mode:		resw	1

	CRT.Font.Demensions:
	CRT.Font.Width:		resb    1
	CRT.Font.Height:	resb    1

	CRT.Cursor.Shape:	resw	1
	CRT.Cursor.State:	resw	1
	CRT.Cursor.Sync:	resw	1
	CRT.Cursor.Pos:
	CRT.Cursor.X:		resb	1
	CRT.Cursor.Y:		resb    1

	%ifdef CRT_BIOS_VIDEO
	%endif

	%ifdef CRT_DIRECT_VIDEO
		CRT.Regen	resw	1
		CRT.Offset:	resw	1
		CRT.Segment:	resw 	1
	%endif

	%ifdef CRT_MOUSE
	%endif

	%ifdef CRT_VESA
	; very temporary, hard coded and reserved SHARED_BUFFER_256 in BSS
		TempBuffer 256
	%endif

	%ifdef CRT_SCROLL_HOOK
	; used internally by CRTWriter to know when to call CRT.Scroll.Hook
		CRT.LINEFLAG:		resw	1
	%endif

	CRT.Initial.Cursor.Shape:	resw 	1
	CRT.Initial.PageAttr:
	CRT.Initial.Attr:		resb	1
	CRT.Initial.Page:		resb	1
	CRT.Initial.Mode:		resw	1

%endif
%endmacro
; -----------------------------------------------------------------------------
%macro CRTScrollHookBSS_INTERNAL 0

CRT.Scroll.Hook:	resw 1

%endmacro
; -----------------------------------------------------------------------------
%macro CRTScrollHook_INTERNAL 0
CRTScrollHook_PROC:
	push	si, di, es, ds
	mov	ds, cs
	mov	es, ds
	xor	al, al
	test	cx, cx
	jnz	%%NotAll
	cmp	dx, [CRT.Max]
	jne	%%NotAll
	inc 	al
%%NotAll:
	; AH is Function, 0=Up, 1=Down, 2=Left, 3=Right, 0xff=Clear
	; AL = 1 then entire screen, AL = 0 only part of the screen
	; CX/DX contain coordinates of the effected screen region
	call	bx
	; ALL registers are restored, either here or up the chain
	pop	ds, es, di, si
	ret
%endmacro
; *****************************************************************************
%imacro CRTScrollHook 1
%ifdef CRT_SCROLL_HOOK
	REQUIRE CRTScrollHook
	REQUIRE CRTScrollHookBSS
	push	bx
	mov	bx, [cs:CRT.Scroll.Hook]
	test	bx,bx
	jz	%%NoCall
	; push	ax
	%ifidni %1, Up
		mov	ah, 1
	%elifidni %1, Down
		mov	ah, 1
	%elifidni %1, Left
		mov	ah, 2
	%elifidni %1, Right
		mov	ah, 3
	%elifidni %1, Clear
		mov	ah, 0xff
	%elifnidni %1, ah
		mov	ah, %1
	%endif
	call	CRTScrollHook_PROC
	; pop	ax
%%NoCall:
	pop	bx
%endif
%endmacro
; -----------------------------------------------------------------------------
%macro CalcVideoPointer_INTERNAL 0

CalcVideoPointer_PROC:

	; ax = XY coor of screen position
	push		cx, dx, ax
	push		ax
	xor		ah, ah
	shl		ax, 1
	mov		di, ax			; di = X bytes
	pop		ax
	mov		cl, ah
	xor		ah, ah
	mov		ch, ah
	mov		al, [CRT.Max.X]
	inc		ax
	shl		ax, 1			; ax = bytes / line
	push		ax
	mul		cx			; ax = chars to start line
	add		di, ax			; ax = bytes to start pos
	add		di, [CRT.Offset]	; di = start offset
	pop		ax
	mov		dx, [CRT.Segment]
	push		dx
	pop		es
	mov		bx, ax
	pop		ax, dx, cx
	; ES:DI Video Pointer
	; BX is bytes per line
	ret

%endmacro
; -----------------------------------------------------------------------------
%macro CRTInitializeState_INTERNAL 0

CRTInitializeState_PROC:
	push			es, di, dx, cx, bx ,ax
	xor			ax, ax
	; Assume at least monochrome MDA
	%ifdef CRT_DIRECT_VIDEO
		mov		[CRT.Segment], word 0xb000
		mov		[CRT.Offset], ax
	%endif
	mov			[CRT.Card], al		; byte CRT_MDA
	mov			[CRT.Cursor.State], ax

	mov			al, 0x40	; mov ax, 0x0040
	mov			es, ax
	mov			al, [es:0x0085]
	mov			[CRT.Font.Height], al
	mov			[CRT.Font.Width], byte 8
	mov			al, [es:0x0084]
	mov			[CRT.Max.Y], al
	mov			ax, [es:0x004a]
	dec			ax
	mov			[CRT.Max.X], al
	%ifdef CRT_DIRECT_VIDEO
		mov		ax, [es:0x004c]
		mov		[CRT.Regen], ax
		mov		ax, [es:0x004e]
		mov		[CRT.Offset], ax
	%endif

	mov			ax, [es:0x0060]
	mov			[CRT.Cursor.Shape], ax
	xor			ah, ah
	mov			al, [es:0x0049]
	mov			[CRT.Mode], ax
	mov			bh, [es:0x0062]
	mov			[CRT.Page], bh
	mov			ah, 0x08
	int			0x10
	mov			[CRT.Attr], ah

	mov			ax, [es:0x0063]
	cmp			ax, 0x3d4
	jne			%%ProbeHardware
	; If color assume at least CGA
	%ifdef CRT_DIRECT_VIDEO
		mov		[CRT.Segment], word 0xb800
	%endif
	mov			[CRT.Card], byte CRT_CGA

%%ProbeHardware:

%ifdef CRT_VESA
%%ProbeVESA:
; Video BIOS - Get VESA SuperVGA information
; Returns AL=0x4f, if function is supported. AH=0x00 for success.
; Requires SuperVGA.
	push			cs
	pop			es
	mov			di, SHARED_BUFFER_256
	mov			ax, 0x4f00
	int			0x10
	mov			dl, CRT_VESA
	cmp			ax, 0x004f
	je			%%Identified
%endif

%ifdef CRT_VGA
%%ProbeVGA:
; Video BIOS - Video Refresh Control, Enable video refresh
; Returns AL=0x12, if function is supported. Requires VGA or better.
	mov			ax, 0x1200
	mov			bl, 0x36
	int			0x10
	mov			dl, CRT_VGA
	cmp			al, 0x12
	je			%%Identified
%endif

%ifdef CRT_MCGA
%%ProbeMCGA:
; Video BIOS - Video Addressing, Enable CPU video addressing
; Returns AL=0x12, if function is supported. Requires MCGA/VGA or better.

	mov			ax, 0x1200
	mov			bl, 0x32
	int			0x10
	mov			dl, CRT_MCGA
	cmp			al, 0x12
	je			%%Identified
%endif

%ifdef CRT_EGA
%%ProbeEGA:
; Video BIOS - Get EGA Info. (AL does not need set, but will clear it anyway)
; If BH is unchanged on return, then EGA or later is not present.
	mov			ax, 0x1200
	mov			bx, 0xff10
	int			0x10
	mov			dl, CRT_EGA
	cmp			bh, 0xff
	jne			%%Identified
%endif

; Not better than CGA
	mov			[CRT.Font.Height], byte 8
	mov			al, 0x0018
	mov			[CRT.Max.Y], al
	jmp			%%Done
%%Identified:
	mov			[CRT.Card], dl

%%Done:
	MouseDataReset

	%ifndef CRT_WIDESCREEN
		%ifdef CRT_DIRECT_VIDEO
			cmp	[CRT.Max.X], byte 80
			jna	%%NotAWideMode
			and	[CRT.Options], byte 11111110b
		%%NotAWideMode:
		%endif
	%endif
	pop			ax, bx, cx, dx, di, es
	ret
%endmacro
; -----------------------------------------------------------------------------
%macro CRTResetMode_INTERNAL 0

CRTResetMode_PROC:
	%ifdef CRT_MODE_CHANGE
		mov		ax, [CRT.Initial.Mode]
		call		CRTTextMode_PROC
	%endif
	push			es
	mov			ax, 0x0040
	push			ax
	pop			es
	mov			ah, [es:0x0062]
	mov			al, [CRT.Initial.Page]
	cmp			ah, al
	je			%%ResetCursor
	; mov			[CRT.Page], al	; handled by CRTIntializeState
	mov			ah, 0x05
	int			0x10
%%ResetCursor:
	mov			ax, [es:0x0060]
	mov			cx, [CRT.Initial.Cursor.Shape]
	cmp			ax, cx
	je			%%Done
	; mov			[CRT.Cursor.Shape], cx	; handled by CRTIntializeState
	mov			ah, 0x01
	int			0x10
%%Done:
	mov			al, [CRT.Initial.Attr]
	mov			[CRT.Initial.Attr], al

	%ifdef CRT_MOUSE
		call 			MouseHide_PROC
	%endif

	call			CRTInitializeState_PROC

	%ifdef CRT_MOUSE
		test			[CRT.Options], byte vcbAutoMouse
		jz			%%NoAutoMouse
		call 			MouseShow_PROC
	%%NoAutoMouse:
	%endif

		pop		es
	ret

%endmacro
; -----------------------------------------------------------------------------
%macro CRTPutChar_INTERNAL 0

CRTPutChar_PROC:

	%ifndef CRT_BIOS_VIDEO
	%elifndef CRT_DIRECT_VIDEO
	%else
		test 	[CRT.Options], byte vcbDirect
		jnz	CRTPutCharDIRECT_PROC
	%endif


%ifdef CRT_BIOS_VIDEO
CRTPutCharBIOS_PROC:
	push			cx, bx
	; mov			bh, [CRT.Page] ; set by caller
	mov			bl, ah		; write char/attr
	mov			ah, 0x02	; move cursor to xy
	int			0x10		; dx=xy, bh=page
	mov			ah, 0x09
	mov			cx, 0x0001
	int			0x10
	pop			bx, cx
	ret
%endif

%ifdef CRT_DIRECT_VIDEO
CRTPutCharDIRECT_PROC:
	push			es, di
	push			ax
	CalcVideoPtr		dx
	pop			ax
	mov			[es:di], ax
	pop			di, es
	ret
%endif

%endmacro
; -----------------------------------------------------------------------------
%macro CRTGetCharAttr_INTERNAL 0

CRTGetCharAttr_PROC:

	%ifndef CRT_BIOS_VIDEO
	%elifndef CRT_DIRECT_VIDEO
	%else
		test 	[CRT.Options], byte vcbDirect
		jnz	CRTGetCharDIRECT_PROC
	%endif


%ifdef CRT_BIOS_VIDEO
CRTGetCharBIOS_PROC:
	%ifdef DOS_BUG_FIXES
		; see -- https://fd.lod.bz/rbil/interrup/video/1008.html
		push			bp, di, si
	%endif
	push			cx, bx
	; mov			bh, [CRT.Page] ; set by caller
	mov			ah, 0x02	; move cursor to xy
	int			0x10		; dx=xy, bh=page
	mov			ah, 0x08
	; mov			cx, 0x0001
	int			0x10
	pop			bx, cx
	%ifdef DOS_BUG_FIXES
		pop			si, di, bp
	%endif
	ret
%endif

%ifdef CRT_DIRECT_VIDEO
CRTGetCharDIRECT_PROC:
	push			es, di
	push			ax
	CalcVideoPtr		dx
	pop			ax
	mov			ax, [es:di]
	pop			di, es
	ret
%endif

%endmacro
; -----------------------------------------------------------------------------
%macro CRTOpenClose_INTERNAL 0

OpenCRT_PROC:
	%ifdef CRT_SCROLL_HOOK
		mov 	[CRT.Scroll.Hook], word 0
	%endif
	; Figure out current state of Video, Mode, MaxXY, etc.
	call	CRTInitializeState_PROC
	; Set Window to Screen Max XY
	call	CRTMaximumWindow_PROC
	; Save initial state for possible restore at program exit or other times
	call	CRTInitializeOriginals_PROC
	; Mouse stuff might exist, might be null. depends on compile options
	MouseInitialize
	ret

CloseCRT_PROC:
	%ifdef CRT_SCROLL_HOOK
		mov 	[CRT.Scroll.Hook], word 0
	%endif
	%ifdef CRT_MOUSE
		MouseFinalize
		and	[CRT.Options], byte !vcbAutoMouse
	%endif
	%ifdef CRT_MODE_CHANGE
		test	[CRT.Options], byte vcbExitMode
		jz	.NoResetMode
		call	CRTResetMode_PROC
		ret
	%endif
.NoResetMode:
	; test vcbExitCursor, maybe reset it.
	ret

CRTMaximumWindow_PROC:
	push		ax
	xor		ax, ax
	mov		[CRT.WindMin], ax
	mov		ax, [CRT.Max]
	mov		[CRT.WindMax], ax
	pop		ax
	ret


CRTInitializeOriginals_PROC:
	push			es, ax, bx
	mov			ax, 0x0040
	mov			es, ax
	mov			ax, [es:0x0060]
	mov			[CRT.Initial.Cursor.Shape], ax
	mov			al, [es:0x0049]
	xor			ah, ah
	mov			[CRT.Initial.Mode], ax
	mov			bh, [es:0x0062]
	mov			[CRT.Initial.Page], bh
	mov			ah, 0x08
	int			0x10
	mov			[CRT.Initial.Attr], ah
	pop			bx, ax, es
	ret

%ifdef CRT_MODE_CHANGE
CRTTextMode_PROC:
	; at present, AH is ignored. That will probably indicate vesa modes

	%ifdef CRT_MOUSE
		call	MouseHide_PROC
	%endif

	push			es, bx
	mov			bx, 0x0040
	mov 			es, bx
	mov			bh, [es:0x0049]
	cmp			bh, al
	je			.Success
	xor			ah, ah
	push			ax, bx
	int			0x10
	call			CRTInitializeState_PROC
	call			CRTMaximumWindow_PROC
	pop			bx, ax
	mov			bh, [es:0x0049]
	cmp			bh, al
	je			.Success
	stc
	jmp			.Done
.Success:
	clc
.Done:
	%ifdef CRT_MOUSE
		pushf
		test			[CRT.Options], byte vcbAutoMouse
		jz			%%NoAutoMouse
		call 			MouseShow_PROC
	%%NoAutoMouse:
		popf
	%endif

	pop			bx, es
	ret
%endif

CRTGetXY_PROC:
BIOSGetXY_PROC:
	push			ax, bx, cx
	mov			bh, [CRT.Page]
	mov			ah, 0x03		; Get cursor shape/pos
	int			0x10
	pop			cx, bx, ax
	; return DX = absolute position
	ret

CRTSetXY_PROC:
BIOSSetXY_PROC:
	; DX = absolute position
	push			ax, bx
	mov			bh, [CRT.Page]
	mov			ah, 0x02
	int			0x10
	pop			bx, ax
	ret

%endmacro
; -----------------------------------------------------------------------------
%imacro CRTInitializeState 0

	call 			CRTInitializeState_PROC

%endmacro
; *****************************************************************************
; Provided an XY position on the screen, it returns a pointer to that address
; in video ram. Mostly for internal Core CRT use.
; MACRO:  CalcVideoPtr 1 or 3
; INPUT:  %1 = XY screen coordinate (prefers ax)
; OUTPUT: %2 = Pointer to video coordinate in ram (prefers es:di)
;	  %3 = bytes per line (prefers bx)
; if only %1 is provided, es:di -> location and ax=bytes per line on return
; *****************************************************************************
%imacro CalcVideoPtr 1-3

%if %0 = 3
	pushndr 	es:di, %2, bx, %3
	pushndr		ax, %1
	setndr		ax, %1
	call 		CalcVideoPointer_PROC
	popndr		ax, %1			; separate NDRs, that way %3
	setndr		%2, es:di, %3, bx	; could be ax on output
	popndr 		es:di, %2, bx, %3
%elif %0 = 1
	push		bx
	setndr		ax, %1
	call 		CalcVideoPointer_PROC
	mov		ax, bx
	pop		bx
%else
	%error CalcVideoPtr is either 1 or 3 parameters
%endif

%endmacro
; *****************************************************************************
%imacro OpenCRT 0-1
%if %0 = 0
	mov [CRT.Options], byte vcbDefaults
%else
	mov [CRT.Options], %1
%endif
	mov [CRT.Tab], word 0x0408
	call	OpenCRT_PROC
%endmacro
; *****************************************************************************
; Restores video settings, to original mode, page, and cursor if needed.
; Registers are not preserved. Automatically called on exit if vcbExitMode
; is set. Automatically,calls CRTProbeSettings_PROC to update data data.
; MACRO:  ResetCRT 0
; INPUT:  none
; OUTPUT: none
; *****************************************************************************
%macro ResetCRT 0
	call			CRTResetMode_PROC
%endmacro
; *****************************************************************************
%imacro CloseCRT 0
	call			CloseCRT_PROC
%endmacro
; *****************************************************************************
; Changes video mode if needed.
; Registers are not preserved. Automatically calls ProbeVideoSettings to
; update data.
; MACRO:  TextMode 1
; INPUT:  %1 = video mode (prefers ax), original returns it to Initial Setting
; OUTPUT: Carry Flag set if error occurred.
; *****************************************************************************
%imacro TextMode 1

%ifdef CRT_MODE_CHANGE
	%ifidni %1, original
		mov		ax, [CRT.Initial.Mode]
	%else
		setndr		ax, %1
	%endif
	call 			CRTTextMode_PROC
%endif

%endmacro
; -----------------------------------------------------------------------------
%ifndef CRT_BIOS_VIDEO
	%ifndef CRT_DIRECT_VIDEO
		%undef INCLUDE_CORE_CRT
	%endif
%endif

%ifndef INCLUDE_CORE_CRT
	%undef CRT_DIRECT_VIDEO
	%undef CRT_BIOS_VIDEO
	%undef CRT_MOUSE
%endif

%ifdef INCLUDE_CORE_CRT
	REQUIRE CRTInitializeState
	REQUIRE CRTOpenClose
	REQUIRE CRTResetMode
	REQUIRE CRTDataBSS
	REQUIRE CRTPutChar
%endif

%ifdef CRT_BIOS_VIDEO
%endif

%ifdef CRT_DIRECT_VIDEO
	REQUIRE CalcVideoPointer
%endif

%ifdef CRT_SCROLL_HOOK
	REQUIRE CRTScrollHook
	REQUIRE CRTScrollHookBSS
%endif

; *****************************************************************************
%else				; if file has already been included
; *****************************************************************************

%ifdef INCLUDE_CORE_CRT
	PUSH_SECTION .proc
		PROVIDE CRTInitializeState
		PROVIDE CalcVideoPointer
		PROVIDE CRTOpenClose
		PROVIDE CRTResetMode
		PROVIDE CRTPutChar
		PROVIDE CRTScrollHook
		PROVIDE CRTGetCharAttr
	POP_SECTION

	PROVIDE_BSS	CRTDataBSS
	PROVIDE_BSS	CRTInitialBSS
	PROVIDE_BSS 	CRTScrollHookBSS

%endif

%endif

