; BSD 3-Clause License
; Copyright (c) 2022-2023, Jerome Shidel

; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:

; 1. Redistributions of source code must retain the above copyright notice, this
;    list of conditions and the following disclaimer.

; 2. Redistributions in binary form must reproduce the above copyright notice,
;    this list of conditions and the following disclaimer in the documentation
;    and/or other materials provided with the distribution.

; 3. Neither the name of the copyright holder nor the names of its
;    contributors may be used to endorse or promote products derived from
;    this software without specific prior written permission.

; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
; DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
; CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

; NASM 2.15.05, or later

; *****************************************************************************
%ifndef CORECRT_BOXLINES_INC_DEFINED
%define CORECRT_BOXLINES_INC_DEFINED
; *****************************************************************************
%macro BoxLineData_INTERNAL 0

BoxLineData_DATA:

.Horizontal:
	db 0xc4	; single horz
		db 0xb3 ; single vert
		db 0xc3, 0xc5, 0xb4 ; connect vert left, center right
		db 0xba ; double vert
		db 0xc7, 0xd7, 0xb6 ; connect vert left, center right
	db 0xcd ; double horz
		db 0xb3 ; single vert
		db 0xc6, 0xd8, 0xb5 ; connect vert left, center right
		db 0xba ; double vert
		db 0xcc, 0xce, 0xb9 ; connect vert left, center right
.Vertical:
	db 0xb3 ; single vert
		db 0xc4 ; horz single
		db 0xc2, 0xc5, 0xc1 ; connect horz single, top, center, bottom
		db 0xcd ; horz double
		db 0xd1, 0xd8, 0xcf ; connect horz double top, center, bottom
	db 0xba ; double vert
		db 0xc4 ; horz single
		db 0xd2, 0xd7, 0xd0 ; connect horz single, top, center, bottom
		db 0xcd ; horz double
		db 0xcb, 0xce, 0xca ; connect horz double top, center, bottom

%endmacro
; -----------------------------------------------------------------------------
%macro BoxFrameData_INTERNAL 0

BoxFrameData_DATA:

.HSVS:		db 0xda, 0xc4, 0xbf, 0xb3, 0xc0, 0xc4, 0xd9
.HSVD:		db 0xd6, 0xc4, 0xb7, 0xba, 0xd3, 0xc4, 0xbd
.HDVS:		db 0xd5, 0xcd, 0xb8, 0xb3, 0xd4, 0xcd, 0xbe
.HDVD:		db 0xc9, 0xcd, 0xbb, 0xba, 0xc8, 0xcd, 0xbc

%endmacro
; -----------------------------------------------------------------------------
%macro DrawLine_INTERNAL 0

DrawLine_PROC:
	push		ax, bx, cx, dx, si, di
	mov		ch, cl
	mov		cl, dl
	xor		di, di
	StackCheck

	%ifdef 	CRT_BIOS_VIDEO
		; Although, when using direct mode, this is not needed.
		; But, it won't hurt much to save and restore the cursor
		; position in direct mode when bios support is included.
		push		ax, cx
		mov		bh, [CRT.Page]
		mov		ah, 0x03
		int		0x10		; get cursor position
		pop		cx, ax
		push		dx		; save cursor position
	%endif

	mov		dx, ax			; requested start xy position
	add		dx, [CRT.WindMin]	; offset to screen xy position
	mov		bl, [CRT.Attr]

	mov		si, BoxLineData_DATA.Horizontal
	test		ch, lnVertical
	jz		%%NotVerticalChars
	mov		si, BoxLineData_DATA.Vertical
%%NotVerticalChars:
	test		ch, lnDouble		; either double horz/vert
	jz		%%NotDoubleLine
	add		si, 9
%%NotDoubleLine:

	mov		bh, [CRT.Page]

	%ifdef CRT_MOUSE
		push	cx
		inc	cl
		test	ch, lnVertical
		jnz	%%NotIsVertical
		mov	ch, dh
		add	cl, dl
		jmp	%%MaybeHideMouse
	%%NotIsVertical:
		mov	ch, cl
		mov	cl, dl
		add	ch, dh
	%%MaybeHideMouse:
		MousePreWrite	dx, cx
		pop	cx
	%endif

%%DoDrawing:
	test		cl, cl
	jz		%%DoneDrawing
	dec		cl

	%ifndef CRT_BIOS_VIDEO
	%elifndef CRT_DIRECT_VIDEO
	%else
		test 	[CRT.Options], byte vcbDirect
		jnz	%%DoDrawing_DIRECT
	%endif

	%ifdef	CRT_BIOS_VIDEO
		mov		ah, 0x02
		int		0x10		; move cursor to xy position
		mov		ah, 0x08
		int		0x10		; fetch char/attr at xy position
		mov		ah, al
	%endif

	%ifndef CRT_BIOS_VIDEO
	%elifndef CRT_DIRECT_VIDEO
	%else
		jmp		%%DoDrawing_DONE
	%endif

	%ifdef	CRT_DIRECT_VIDEO
%%DoDrawing_DIRECT:
		push		es, di
		CalcVideoPtr 	dx
		mov	ah, 	[es:di]
		pop		di, es
	%endif

%%DoDrawing_DONE:

	push		bx			; save page/textattr
	mov		al, [si]
	test		ch, lnNoConnect
	jnz		%%NoConnect

	xor		bx, bx
	cmp		ah, [si + 1]
	jne		%%SecondTest
	mov		bx, 2
	jmp		%%ChangeChar
%%SecondTest:
	cmp		ah, [si + 5]
	jne		%%NoChangeChar
	mov		bx, 6
%%ChangeChar:
	test		di, di			; is it the first char
	jz		%%ChangeCharDone
	inc		bx
	test		cl, cl			; is it the last char
	jnz		%%ChangeCharDone
	inc		bx
%%ChangeCharDone:
	mov		al, [si+bx]
%%NoChangeChar:

%%NoConnect:
	pop		bx			; restore page/textattr

	%ifndef CRT_BIOS_VIDEO
	%elifndef CRT_DIRECT_VIDEO
	%else
		test 	[CRT.Options], byte vcbDirect
		jnz	%%CRTPutChar_DIRECT
	%endif

	%ifdef	CRT_BIOS_VIDEO
		push		cx
		mov		ah, 0x09
		mov		cx, 0x0001
		int		0x10		; pu line/box char on screen
		pop		cx
	%endif

	%ifndef CRT_BIOS_VIDEO
	%elifndef CRT_DIRECT_VIDEO
	%else
		jmp		%%CRTPutChar_DONE
	%endif

	%ifdef	CRT_DIRECT_VIDEO
	%%CRTPutChar_DIRECT:
		push	es, di
		mov	ah, bl
		push	ax
		CalcVideoPtr dx
		pop	ax
		mov	[es:di], ax
		pop	di, es
	%endif

%%CRTPutChar_DONE:
	inc		di			; total chars written in line
	cmp		dh, [CRT.WindMax.Y]
	ja		%%OutOfBounds
	cmp		dl, [CRT.WindMax.X]
	ja		%%OutOfBounds
	test		ch, lnVertical
	jz		%%NotVerticalMove
	inc		dh
	jmp		%%DoDrawing

%%NotVerticalMove:
	inc		dl
	jmp		%%DoDrawing

%%OutOfBounds:
%%DoneDrawing:

	%ifdef 	CRT_BIOS_VIDEO
		pop		dx		; done in early ifdef
		mov		ah, 0x02
		int		0x10		; restore cursor position
	%endif
	%ifdef CRT_MOUSE
		MousePostWrite
	%endif
	pop		di, si, dx, cx, bx, ax
	ret

%endmacro
; -----------------------------------------------------------------------------
%macro DrawBox_INTERNAL 0

DrawBox_PROC:
	push		di, si, ax, bx, cx, dx

	StackCheck

	mov		si, BoxFrameData_DATA.HSVS
	test		cx, lnDouble
	jz		%%FrameSet
	mov		si, BoxFrameData_DATA.HDVD
	cmp		cx, lnDoubleVert
	ja		%%FrameSet
	mov		si, BoxFrameData_DATA.HDVS
	jb		%%FrameSet
	mov		si, BoxFrameData_DATA.HSVD
%%FrameSet:

	add		dx, 0x0101
	sub		dx, ax
	mov		cx, dx
	add		ax, [CRT.WindMin]
	mov		dx, ax			; dx = real xy, cx = wide/high

	%ifdef CRT_MOUSE
		push		cx
		add		cx, dx
		MousePreWrite 	dx, cx
		pop		cx
	%endif

	%ifdef 	CRT_BIOS_VIDEO
		push		dx, cx
		mov		bh, [CRT.Page]
		mov		ah, 0x03
		int		0x10		; get cursor position
		mov		ax, dx
		pop		cx, dx
		push		ax		; save cursor position for later
	%endif

	mov		ah, [CRT.Attr]
	cld
%%DrawTop:
	call		%%DrawHorz
	lodsb
%%DrawSides:
	dec		ch
	cmp		ch, 1
	jna		%%DrawBottom
	inc		dh
	push		dx
	call		%%DrawChar
	add		dl, cl
	dec		dl
	call		%%DrawChar
	pop		dx

	jmp		%%DrawSides
%%DrawBottom:
	dec		ch
	jnz		%%DrawDone
	inc		dh
	call		%%DrawHorz
	jmp		%%DrawDone

%%DrawHorz:
	push		cx, dx
	lodsb
	call		%%DrawChar
	inc		dl
	dec		cl
	lodsb
%%DrawHorzLoop:
	cmp		cl, 1
	jna		%%DrawHorzEnd
	call		%%DrawChar
	inc		dl
	dec		cl
	jmp		%%DrawHorzLoop
%%DrawHorzEnd:
	lodsb
	cmp		cl, 1
	jne		%%DrawHorzLoopDone
	call		%%DrawChar
%%DrawHorzLoopDone:
	pop		dx, cx
	ret
%%DrawChar:
	cmp		dl, [CRT.WindMax.X]
	ja		%%OutOfBounds
	cmp		dh, [CRT.WindMax.Y]
	ja		%%OutOfBounds
	push		ax
	call		CRTPutChar_PROC
	pop		ax
%%OutOfBounds:
	ret
%%DrawDone:

	%ifdef 	CRT_BIOS_VIDEO
		pop		dx		; was ax in prior ifdef
		mov		ah, 0x02
		int		0x10		; restore cursor position
	%endif

	%ifdef CRT_MOUSE
		MousePostWrite
	%endif

	pop		dx, cx, bx, ax, si, di
	ret

%endmacro
; *****************************************************************************
; Display a horizontal or vertical line
; MACRO:  DrawLine 3
; INPUT:  %1 = XY, %2 = Count, %3 = Options (Prefers AX, DX, CX)
; OUTPUT: none
; REGS:	  ax, bx, cx, dx
; *****************************************************************************
%imacro DrawLine 3

	; REQUIRE		WriteHexByte
	; REQUIRE		WriteHexWord

	REQUIRE		BoxLineData
	REQUIRE		DrawLine

	pushndr			ax, %1, dx, %2, cx, %3
	setndr			ax, %1, dx, %2, cx, %3
	call 			DrawLine_PROC
	popndr			ax, %1, dx, %2, cx, %3


%endmacro
; *****************************************************************************
; Display a Box
; MACRO:  DrawBox 3
; INPUT:  %1 = MinXY, %2 = MaxXY, %3 = Options (Prefers AX, DX, CX)
; OUTPUT: none
; REGS:	  ax, bx, cx, dx
; *****************************************************************************
%imacro DrawBox 3

	REQUIRE		BoxFrameData
	REQUIRE		DrawBox

	pushndr		ax, %1, dx, %2, cx, %3
	setndr		ax, %1, dx, %2, cx, %3
	call 		DrawBox_PROC
	popndr		ax, %1, dx, %2, cx, %3


%endmacro
; -----------------------------------------------------------------------------
%else				; if file has already been included
; *****************************************************************************
	PUSH_SECTION .proc
		PROVIDE		DrawLine
		PROVIDE		DrawBox
	POP_SECTION

	PUSH_SECTION .data
		PROVIDE		BoxLineData
		PROVIDE		BoxFrameData
	POP_SECTION

%endif
